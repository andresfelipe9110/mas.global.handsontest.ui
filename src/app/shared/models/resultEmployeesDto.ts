import { EmployeeDto } from './employeeDto';

export class ResultEmployeesDto {
    employeesDto: EmployeeDto[];
    message: string;
    result: string;
}
