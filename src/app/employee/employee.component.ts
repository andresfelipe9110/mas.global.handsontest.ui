import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeServiceService } from '../shared/services/employee-service.service';
import { EmployeeDto } from '../shared/models/employeeDto';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  form: FormGroup;

  employeesList: EmployeeDto[];

  loader: boolean;

  showError: boolean;

  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeServiceService,
  ) {
    this.form = this.fb.group({
      search: [''],
    });
  }

  ngOnInit() {
  }

  searchEmployees(): void {
    const formValues = this.form.value;
    this.employeesList = [];
    this.showError = false;

    if (formValues && formValues.search !== '' && !isNaN(Number(formValues.search))) {
      this.loader = true;
      this.employeeService.getEmployeeById(Number(formValues.search)).subscribe((result) => {
        if (result && result.result === 'Success' && result.employeeDto) {
          this.employeesList.push(result.employeeDto);
        } else if (result.result === 'Error') {
          this.showError = true;
          this.errorMessage = result.message;
        }
        this.loader = false;
      });
    } else if (formValues && formValues.search === '') {
      this.loader = true;
      this.employeeService.getAllEmployees().subscribe((result) => {
        if (result && result.result === 'Success' && result.employeesDto) {
          this.employeesList = result.employeesDto;
        } else if (result.result === 'Error') {
          this.showError = true;
          this.errorMessage = result.message;
        }
        this.loader = false;
      });
    }
  }

  clear() {
    this.loader = false;
    this.showError = false;
    this.employeesList = [];
    this.form.get('search').setValue('');
  }

}
