import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs/internal/Observable';
import { GlobalVariables } from '../utils/globalVariables';
import { ResultEmployeesDto } from '../models/resultEmployeesDto';
import { ResultEmployeeDto } from '../models/resultEmployeeDto';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  resultEmployees: Observable<ResultEmployeesDto>;

  resultEmployee: Observable<ResultEmployeeDto>;

  constructor(
    private http: HttpClient
  ) { }

  getAllEmployees(): Observable<ResultEmployeesDto> {
    const urlApi = `${GlobalVariables.API_EMPLOYEES_URL}/GetAllEmployees`;
    return (this.resultEmployees = this.http.get<ResultEmployeesDto>(urlApi));
  }

  getEmployeeById(id: number): Observable<ResultEmployeeDto> {
    const urlApi = `${GlobalVariables.API_EMPLOYEES_URL}/GetEmployeeById?id=${id}`;
    return (this.resultEmployee = this.http.get<ResultEmployeeDto>(urlApi));
  }
}
