import { EmployeeDto } from './employeeDto';

export class ResultEmployeeDto {
    employeeDto: EmployeeDto;
    message: string;
    result: string;
}
